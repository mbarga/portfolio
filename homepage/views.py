from django.shortcuts import render


def index(request):
    return render(request, 'index.html')


def home(request):
    return render(request, 'home.html')


def contact(request):
    return render(request, 'contact.html')


def cv(request):
    return render(request, 'cv.html')


def projects(request):
    return render(request, 'projects.html')


def error404(request):
    return render(request, '404.html')


def error500(request):
    return render(request, '500.html')
