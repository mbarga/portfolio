from django.conf.urls import patterns, url
from django.conf import settings
from django.conf.urls.static import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.views.generic import TemplateView

from . import views

handler500 = views.error500

urlpatterns = patterns('',
                       url(r'^profile/$', views.index, name="index"),
                       url(r'^profile/home/$', views.home, name="home"),
                       url(r'^profile/contact/$', views.contact, name="contact"),
                       url(r'^profile/cv/$', views.cv, name="cv"),
                       url(r'^profile/projects/$', views.projects, name="projects"),
                       ) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)   # noqa

urlpatterns += staticfiles_urlpatterns()
