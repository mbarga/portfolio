from django.core.wsgi import get_wsgi_application
from dj_static import Cling, MediaCling

import os,sys

ROOT_PATH = os.path.dirname(__file__)
path = ''
if path not in sys.path:
    sys.path.append(ROOT_PATH)

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "production")

application = Cling(MediaCling(get_wsgi_application()))
